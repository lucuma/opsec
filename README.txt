`evil_ips.txt` contiene una lista de IPs malvadas que deben ser bloqueadas.
Estas IPs vienen de intentos de ataques reales registrados en los logs.

`evil_ips.txt.gz` es la versión comprimida de ese archivo y el que finalmente se usa.

Si actualizas `evil_ips.txt`:

1. no olvides volver a generar `evil_ips.txt.gz` con el comando

	gzip -k evil_ips.txt

2. mantenlo siempre ordenado numéricamente
